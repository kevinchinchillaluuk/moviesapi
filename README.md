# Movies RESt API

To start the project 

1. `docker-compose up` to start mysql database
2. `npm install` to add dependencies
3. `npx sequelize-cli db:create` to create db specify in config file
3. `npx sequelize-cli db:migrate` to create tables in the database
4. `npx sequelize-cli db:seed:all` to seed data
5. `npm start`