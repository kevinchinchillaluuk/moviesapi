require("dotenv").config();
const express = require("express");
const initRouter = require("./src/routes/index");
const errorHandler = require("./src/middlewares/errorHandler");
const passport = require("passport");
const NotFoundError = require("./src/errors/notFoundError");

const app = express();

app.use(express.json());

app.use(passport.initialize());

initRouter(app);

app.use((req, res, next) => {
  throw new NotFoundError(`Cannot ${req.method} ${req.path}`);
})

app.use(errorHandler);

app.listen(8080, () => {
  console.log("Server up");
});


