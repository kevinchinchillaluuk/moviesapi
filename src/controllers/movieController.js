const { validationResult, matchedData } = require("express-validator");
const ValidationError = require("../errors/validationError");
const MovieService = require("../services/movieService");
const bannedMiddleware = require("../middlewares/bannedMiddleware");
const sortMiddleware = require("../middlewares/sortMiddleware");
const paginationValidator = require("../validators/pagination");
const {movieValidator,updatedMovieValidator} = require("../validators/movie");
const checkRoleMiddleware = require("../middlewares/checkRoleMiddleware");
const { authJwtMiddleware } = require("../middlewares/passportMiddleware");
const paginationResponse = require("../responses/paginationResponse");
const { likeMovieResponse, disLikeMovieResponse } = require("../responses/movieResponse");
const { sortMoviesValidator } = require("../validators/sortValidator");

/**
 * Retrieves a list of movies from database
 * @param {Object} req
 * @param {number} [req.query.page] - Page number to allow pagination.
 * @param {number} [req.query.perPage] - Number of movies per page.
 * @param {string} [req.query.sort] - Used to describe sorting rules.
 * @param {*} res
 * @param {*} next
 */
const getMovies = async (req, res, next) => {
  const { page = 1, perPage = 10 } = matchedData(req);
  const { field = "title", direction = "ASC" } = req.sort;

  try {
    const { count, rows } = await MovieService.findMovies(page,perPage,field,direction);
    const response = paginationResponse(rows, count, page, perPage);

    res.status(200).json(response);
  } catch (error) {
    next(error);
  }
};

/**
 * Creates a new movie
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createMovie = async (req, res, next) => {
  const errors = validationResult(req);
  try {
    if (!errors.isEmpty()) {
      throw new ValidationError(errors.array());
    }
    let newMovie = await MovieService.createMovie(req.body);

    res.status(201).json(newMovie);
  } catch (error) {
    next(error);
  }
};

/**
 * Updates an existing movie
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const updateMovie = async (req, res, next) => {
  const errors = validationResult(req);
  const updatedMovie = matchedData(req, { includeOptionals: false });
  const { movieId } = req.params;
  try {
    if (!errors.isEmpty()) {
      throw new ValidationError(errors.array());
    }
    const movie = await MovieService.updateMovie(movieId, updatedMovie);

    res.status(200).json(movie);
  } catch (error) {
    next(error);
  }
};

/**
 * Deletes an exsiting movie
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const deleteMovie = async (req, res, next) => {
  let { movieId } = req.params;
  try {
    await MovieService.deleteMovie(movieId);

    res.status(204).send();
  } catch (error) {
    next(error);
  }
};

/**
 * Creates an entry in userLikes table and increment likecount in the specify movie.
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const likeMovie = async (req, res, next) => {
  const { movieId } = req.params;
  const { id: userId } = req.user;
  try {
    await MovieService.likeMovie(movieId, userId);

    res.status(200).json(likeMovieResponse);
  } catch (error) {
    next(error);
  }
};

/**
 * Deletes an entry in userLikes table and decrements likecount in the specify movie.
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
const disLikeMovie = async (req, res, next) => {
  const { movieId } = req.params;
  const { id: userId } = req.user;
  try {
    await MovieService.disLikeMovie(movieId, userId);

    res.status(200).json(disLikeMovieResponse);
  } catch (error) {
    next(error);
  }
};

module.exports = {
  getMovies: [
    authJwtMiddleware,
    bannedMiddleware,
    paginationValidator,
    sortMoviesValidator,
    sortMiddleware,
    getMovies,
  ],
  createMovie: [
    authJwtMiddleware,
    checkRoleMiddleware,
    movieValidator,
    createMovie,
  ],
  updateMovie: [
    authJwtMiddleware,
    checkRoleMiddleware,
    updatedMovieValidator,
    updateMovie,
  ],
  deleteMovie: [authJwtMiddleware, checkRoleMiddleware, deleteMovie],
  likeMovie: [authJwtMiddleware, bannedMiddleware, likeMovie],
  disLikeMovie: [authJwtMiddleware, bannedMiddleware, disLikeMovie],
};
