const { validationResult, matchedData } = require("express-validator");
const jwt = require("jsonwebtoken");
const ValidationError = require("../errors/validationError");
const UserService = require("../services/userService");
const { authLocalMiddleware, authJwtMiddleware } = require("../middlewares/passportMiddleware");
const bannedMiddleware = require("../middlewares/bannedMiddleware");
const paginationValidator = require("../validators/pagination");
const userValidator = require("../validators/user");
const checkRoleMiddleware = require("../middlewares/checkRoleMiddleware");
const sortMiddleware = require("../middlewares/sortMiddleware");
const { sortUsersValidator, sortMoviesValidator } = require("../validators/sortValidator");

/**
 * Retrieves a list of movies from database
 * @param {Object} req
 * @param {number} [req.query.page] - Page number to allow pagination.
 * @param {number} [req.query.perPage] - Number of users per page.
 * @param {string} [req.query.sort] - Used to describe sorting rules.
 * @param {*} res
 * @param {*} next
 */
const getUsers = async (req, res, next) => {
  try {
    const { page = 1, perPage = 10 } = matchedData(req);
    const { field = "email", direction = "ASC" } = req.sort;

    const response = await UserService.findUsers(
      page,
      perPage,
      field,
      direction
    );

    res.status(200).json(response);
  } catch (error) {
    next(error);
  }
};

/**
 * Retrieves user's profile information
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const getUserProfile = async (req, res, next) => {
  try {
    const { id: userId } = req.user;
    const user = await UserService.findUser(userId);
    res.status(200).json(user);
  } catch (error) {
    next(error);
  }
};

/**
 * Creates a new user with admin role
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createAdmin = async (req, res, next) => {
  const errors = validationResult(req);
  const body = matchedData(req);
  body.role = "admin";

  try {
    if (!errors.isEmpty()) {
      throw new ValidationError(errors.array());
    }

    const response = await UserService.createUser(body);

    res.status(201).json(response);
  } catch (error) {
    next(error);
  }
};

/**
 * Creates a new user
 * @param {Object} req
 * @param {string} req.body.email - User's email.
 * @param {string} req.body.name - User's name.
 * @param {string} req.body.password - User's password
 * @param {*} res
 * @param {*} next
 */
const createUser = async (req, res, next) => {
  const errors = validationResult(req);
  const body = matchedData(req);
  body.role = "regular";

  try {
    if (!errors.isEmpty()) {
      throw new ValidationError(errors.array());
    }
    const response = await UserService.createAdmin(body);

    res.status(201).json(response);
  } catch (error) {
    next(error);
  }
};

/**
 * Deletes a specify user
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const deleteUser = async (req, res, next) => {
  const { userId } = req.params;
  try {
    await UserService.deleteUser(userId);
    res.status(204).send();
  } catch (error) {
    next(error);
  }
};

/**
 * Retrieves a list of movies liked by specific user
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const getMoviesLikedByUser = async (req, res, next) => {
  try {
    const { userId } = req.params;
    const { page = 1, perPage = 10 } = matchedData(req);
    const { field = "title", direction = "ASC" } = req.sort;

    const response = await UserService.findMoviesLikedByUser(
      userId,
      page,
      perPage,
      field,
      direction
    );

    res.status(200).json(response);
  } catch (error) {
    next(error);
  }
};

/**
 * Retrieves JWT to authenticate users
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const login = (req, res, next) => {
  try {
    const response = UserService.getAccessToken(req.user);
    res.status(200).json(response);
  } catch (error) {
    throw error;
  }
};

/**
 * Logsout current user
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const logout = async (req, res, next) => {
  const token = req.get("Authorization").split(" ")[1];
  const response = UserService.logout(token);
  res.status(201).json(response);
};

/**
 * Ban a specific user(Admin only)
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const banUser = async (req, res, next) => {
  try {
    const { userId } = req.params;
    const banned = true;

    const response = await UserService.banUser(userId, banned);
    res.status(200).json(response);
  } catch (error) {
    next(error);
  }
};

/**
 * Unban a specific user(Admin only)
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const unBanUser = async (req, res, next) => {
  try {
    const { userId } = req.params;
    const banned = false;

    const response = await UserService.banUser(userId, banned);
    res.status(200).json(response);
  } catch (error) {
    next(error);
  }
};

module.exports = {
  getUsers: [
    authJwtMiddleware,
    checkRoleMiddleware,
    paginationValidator,
    sortUsersValidator,
    sortMiddleware,
    getUsers,
  ],
  getUserProfile: [authJwtMiddleware, bannedMiddleware, getUserProfile],
  createAdmin: [
    authJwtMiddleware,
    checkRoleMiddleware,
    userValidator,
    createAdmin,
  ],
  createUser: [userValidator, createUser],
  deleteUser: [authJwtMiddleware, checkRoleMiddleware, deleteUser],
  getMoviesLikedByUser: [
    authJwtMiddleware,
    checkRoleMiddleware,
    paginationValidator,
    sortMoviesValidator,
    sortMiddleware,
    getMoviesLikedByUser,
  ],
  login: [authLocalMiddleware, bannedMiddleware, login],
  logout: [authJwtMiddleware, logout],
  banUser: [authJwtMiddleware, checkRoleMiddleware, banUser],
  unBanUser: [authJwtMiddleware, checkRoleMiddleware, unBanUser],
};
