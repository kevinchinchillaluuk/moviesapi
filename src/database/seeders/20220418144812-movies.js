"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
   
    await queryInterface.bulkInsert("Movies", [
      {
        title: "Titanic",
        description:
          "A seventeen-year-old aristocrat falls in love with a kind but poor artist aboard the luxurious, ill-fated R.M.S. Titanic.",
        imageUrl:
          "https://cdn.hobbyconsolas.com/sites/navi.axelspringer.es/public/styles/1200/public/media/image/2016/12/titanic.jpg?itok=rd2m-cqM",
        releaseDate: "1997-04-07",
        createdAt: new Date(),
        updatedAt: new Date()

      },
      {
        title: "Iron Man",
        description: "After being held captive in an Afghan cave, billionaire engineer Tony Stark creates a unique weaponized suit of armor to fight evil.",
        imageUrl: "https://upload.wikimedia.org/wikipedia/en/0/02/Iron_Man_%282008_film%29_poster.jpg",
        releaseDate: "2008-04-10",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: "The batman",
        description: "When the Riddler, a sadistic serial killer, begins murdering key political figures in Gotham, Batman is forced to investigate the city's hidden corruption and question his family's involvement.",
        imageUrl: "https://pics.filmaffinity.com/The_Batman-449856406-large.jpg",
        releaseDate: "2022-02-14",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: "The Last Samurai",
        description: "An American military advisor embraces the Samurai culture he was hired to destroy after he is captured in battle.",
        imageUrl: "https://image.tmdb.org/t/p/original/lsasOSgYI85EHygtT5SvcxtZVYT.jpg",
        releaseDate: "2003-06-15",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: "Avatar",
        description: "A paraplegic Marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home.",
        imageUrl: "https://mattcanada.files.wordpress.com/2016/04/avatar.jpg",
        releaseDate: "2010-10-29",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: "Pulp Fiction",
        description: "The lives of two mob hitmen, a boxer, a gangster and his wife, and a pair of diner bandits intertwine in four tales of violence and redemption.",
        imageUrl: "https://pics.filmaffinity.com/Pulp_Fiction-210382116-large.jpg",
        releaseDate: "1994-09-18",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: "Independence Day",
        description: "The aliens are coming and their goal is to invade and destroy Earth. Fighting superior technology, mankind's best weapon is the will to survive.",
        imageUrl: "https://pics.filmaffinity.com/Independence_Day-797222535-mmed.jpg",
        releaseDate: "1996-12-20",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: "The Godfather",
        description: "The aging patriarch of an organized crime dynasty in postwar New York City transfers control of his clandestine empire to his reluctant youngest son.",
        imageUrl: "https://www.imdb.com/title/tt0068646/mediaviewer/rm746868224/",
        releaseDate: "1972-12-21",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: "The Lord of the Rings: The Return of the King",
        description: "Gandalf and Aragorn lead the World of Men against Sauron's army to draw his gaze from Frodo and Sam as they approach Mount Doom with the One Ring.",
        imageUrl: "https://www.imdb.com/title/tt0167260/mediaviewer/rm584928512/",
        releaseDate: "2003-09-01",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: "Forrest Gump",
        description: "The presidencies of Kennedy and Johnson, the Vietnam War, the Watergate scandal and other historical events unfold from the perspective of an Alabama man with an IQ of 75, whose only desire is to be reunited with his childhood sweetheart.",
        imageUrl: "https://www.imdb.com/title/tt0109830/mediaviewer/rm1954748672/",
        releaseDate: "1994-02-10",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: "Fight Club",
        description: "An insomniac office worker and a devil-may-care soap maker form an underground fight club that evolves into much more.",
        imageUrl: "https://www.imdb.com/title/tt0137523/mediaviewer/rm2110056193/",
        releaseDate: "1999-12-09",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: "Inception",
        description: "A thief who steals corporate secrets through the use of dream-sharing technology is given the inverse task of planting an idea into the mind of a C.E.O., but his tragic past may doom the project and his team to disaster.",
        imageUrl: "https://www.imdb.com/title/tt1375666/mediaviewer/rm3426651392/",
        releaseDate: "2010-08-07",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: "Star Wars: Episode V - The Empire Strikes Back",
        description: "After the Rebels are brutally overpowered by the Empire on the ice planet Hoth, Luke Skywalker begins Jedi training with Yoda, while his friends are pursued across the galaxy by Darth Vader and bounty hunter Boba Fett.",
        imageUrl: "https://www.imdb.com/title/tt0080684/mediaviewer/rm3114097664/",
        releaseDate: "1980-11-15",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: "Interstellar",
        description: "A team of explorers travel through a wormhole in space in an attempt to ensure humanity's survival.",
        imageUrl: "https://www.imdb.com/title/tt0816692/mediaviewer/rm4043724800/",
        releaseDate: "2014-08-24",
        createdAt: new Date(),
        updatedAt: new Date()

      },
      {
        title: "The Matrix",
        description: "When a beautiful stranger leads computer hacker Neo to a forbidding underworld, he discovers the shocking truth--the life he knows is the elaborate deception of an evil cyber-intelligence.",
        imageUrl: "https://www.imdb.com/title/tt0133093/mediaviewer/rm525547776/",
        releaseDate: "1999-03-31",
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ]);
  },

  async down(queryInterface, Sequelize) {
    
    await queryInterface.bulkDelete('Movies', null, {});
  },
};
