'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    
     await queryInterface.bulkInsert('Users', [{
      name: "admin",
      email: "admin@admin.com",
      password: "$2a$12$ZAM3ASETnZSp4TnhPjoSE.igLto4Yw2olipWn.P.8shbsU6KIGbgC",
      banned: false,
      role: "admin"
    }]);
   
  },

  async down (queryInterface, Sequelize) {
     await queryInterface.bulkDelete('Users', null, {}); 
  }
};
