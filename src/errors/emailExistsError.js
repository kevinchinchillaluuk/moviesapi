
class EmailExistsError extends Error {
    constructor(){
        super("Email aready exists");
        this.statusCode = 422;
        this.codeError = "Unprocessable Entity";
    }
}

module.exports = EmailExistsError;