
class ForbiddenError extends Error {
    constructor(message) {
        super(message || "Not allowed to access this resource");
        this.statusCode = 403;
        this.codeError = "Forbidden"
    }
}

module.exports = ForbiddenError;