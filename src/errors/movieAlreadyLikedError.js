
class MovieAlreadyLikedError extends Error {
    constructor() {
        super("Movie already liked");
        this.statusCode = 400;
        this.codeError = "Bad Request"
    }
}

module.exports = MovieAlreadyLikedError;