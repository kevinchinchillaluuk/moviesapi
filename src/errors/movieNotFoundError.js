const NotFoundError = require("./notFoundError");

class MovieNotFoundError extends NotFoundError {
    constructor() {
        super("Movie not found");
    }
}

module.exports = MovieNotFoundError;