
class MovieNotLikedError extends Error {
    constructor() {
        super("Movie not liked");
        this.statusCode = 400;
        this.codeError = "Bad Request"
    }
}

module.exports = MovieNotLikedError;