
class NotFoundError extends Error {
    constructor(message) {
        super(message);
        this.statusCode = 404;
        this.codeError = "Not found"
    }
}

module.exports = NotFoundError;