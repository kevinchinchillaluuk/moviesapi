

class ValidationError extends Error {
    constructor(errors){
        super("Validation failed");
        this.statusCode = 422;
        this.codeError = "Unprocessable Entity";
        this.errors = errors;
    }
}

module.exports = ValidationError;