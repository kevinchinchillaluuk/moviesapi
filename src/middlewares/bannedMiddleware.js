const User = require("../models").User;
const ForbiddenError = require("../errors/forbiddenError");

module.exports = async (req, res, next) => {
  try {
    let user = await User.findByPk(req.user.id);
    //console.log(user)
    if (user.banned) {
      throw new ForbiddenError("Banned user");
    }
    next();
  } catch (error) {
      next(error);
  }
};
