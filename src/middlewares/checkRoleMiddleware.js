const ForbiddenError = require("../errors/forbiddenError");

module.exports = (req, res, next) => {
  if (req.user.role !== "admin") {
    throw new ForbiddenError();
  }
  next();
};
