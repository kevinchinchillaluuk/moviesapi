
module.exports = (error, req, res, next) => {
    let { message } = error;
    console.error(error);
    error.statusCode ? null : (error.statusCode = 500);
    res.status(error.statusCode).json({ message: message, ...error });
  }