const passport = require("passport");
const LocalStrategy = require("passport-local");
const JWTstrategy = require("passport-jwt").Strategy;
const ExtractJWT = require("passport-jwt").ExtractJwt;
const { User, Token } = require("../models");

passport.use(
  new LocalStrategy(
    { usernameField: "email", passwordField: "password" },
    async (username, password, done) => {
      try {
        let user = await User.findOne({ where: { email: username } });

        if (!user) {
          return done(null, false);
        }
        let isValid = await user.verifyPassword(password);

        if (!isValid) {
          return done(null, false);
        }

        return done(null, user);
      } catch (error) {
        done(error);
      }
    }
  )
);

passport.use(
  new JWTstrategy(
    {
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
      secretOrKey: process.env.JWT_SECRET,
    },
    async (token, done) => {
      try {
        let user = await User.findByPk(token.user.id);
        if (!user) {
          return done(null, false);
        }
        let isBlackListed = await Token.findOne({ where: { jti: token.jti } });
        if (isBlackListed) {
          return done(null, false);
        }

        return done(null, token.user);
      } catch (error) {
        done(error, false);
      }
    }
  )
);

exports.authLocalMiddleware = passport.authenticate("local", { session: false });
exports.authJwtMiddleware = passport.authenticate("jwt", { session: false });
