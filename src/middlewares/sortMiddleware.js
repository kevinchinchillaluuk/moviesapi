const { validationResult, matchedData } = require("express-validator");

module.exports = (req, res, next) => {
  const { sort = "" }  = matchedData(req);

  const regex = /(^-)?(\w+)/;
  const groups = sort.match(regex);

  let { 1: direction, 2: field } = groups || [,,,];
  direction = direction === "-" ? "DESC" : "ASC";
  
  req.sort = { field: field, direction: direction };
  next();
};
