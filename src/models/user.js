'use strict';
const bcrypt = require("bcryptjs");
const {Model} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      User.belongsToMany(models.Movie, { through: models.UserLikes });
      models.Movie.belongsToMany(User, { through: models.UserLikes });
    }

    async verifyPassword(password) {
      return await bcrypt.compare(password, this.password);
    }
  }
  User.init({
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    banned: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      allowNull: false,
    },
    role: {
      type: DataTypes.ENUM,
      values: ["regular", "admin"],
      allowNull: false,
    },
  }, {
    sequelize,
    modelName: 'User',
    timestamps: false
  });
  return User;
};