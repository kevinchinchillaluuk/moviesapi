const disLikeMovieResponse = { message: "Movie disliked" };
const likeMovieResponse = { message: "Movie liked" };

module.exports = { disLikeMovieResponse, likeMovieResponse };
