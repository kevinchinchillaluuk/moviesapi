module.exports = (itemsList, totalItems, page, perPage) => {
  const response = {
    data: itemsList,
    pagination: {
      page: page,
      itemsPerPage: perPage,
      totalItems: totalItems,
      totalPages: Math.ceil(totalItems / perPage),
    },
  };
  return response;
};
