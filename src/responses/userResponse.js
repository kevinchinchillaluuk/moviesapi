const userRegisteredResponse = (userId, token) => {
  const response = {
    message: "User registered",
    userId: userId,
    token: token,
  };
  return response;
};

const userAccessTokenResponse = (userId, token) => {
  const response = {
    userId: userId,
    token: token,
  };
  return response;
};

const userBannedResponse = (userId, banned) => {
  const status = banned ? "banned" : "unbanned";
  const response = { message: `User ${status}`, userId: userId };
  return response;
};

module.exports = {
  userRegisteredResponse,
  userAccessTokenResponse,
  userBannedResponse,
};
