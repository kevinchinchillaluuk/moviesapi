const { Router } = require("express");
const userRouter = require("./userRoutes");
const movieRouter = require("./movieRoutes");

const initRouter = (app) => {
  const apiRouter = Router();

  userRouter.init(apiRouter);
  movieRouter.init(apiRouter);
  
  app.use("/api/v1/", apiRouter);

};

module.exports = initRouter;
