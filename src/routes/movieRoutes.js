const { Router } = require("express");
const movieController = require("../controllers/movieController");

const init = (apiRouter) => {
  const movieRouter = Router();
  
  movieRouter.get("/", movieController.getMovies);
  movieRouter.post("/", movieController.createMovie);
  movieRouter.patch("/:movieId", movieController.updateMovie);
  movieRouter.delete("/:movieId", movieController.deleteMovie);
  movieRouter.post("/:movieId/like", movieController.likeMovie);
  movieRouter.post("/:movieId/dislike", movieController.disLikeMovie);

  apiRouter.use("/movies", movieRouter);
};

module.exports = { init };
