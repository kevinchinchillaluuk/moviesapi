const { Router } = require("express");
const userController = require("../controllers/userController");

const init = (apiRouter) => {
  const userRouter = Router();

  userRouter.post("/login", userController.login);
  userRouter.post("/logout", userController.logout);
  userRouter.get("/", userController.getUsers);
  userRouter.get("/profile", userController.getUserProfile);
  userRouter.get("/:userId/movies", userController.getMoviesLikedByUser);
  userRouter.post("/admin", userController.createAdmin);
  userRouter.post("/register", userController.createUser);
  userRouter.post("/:userId/ban", userController.banUser);
  userRouter.post("/:userId/unban", userController.unBanUser);
  userRouter.delete("/:userId", userController.deleteUser);

  apiRouter.use("/users", userRouter);
};

module.exports = { init };

