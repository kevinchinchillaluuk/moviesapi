const MovieNotFoundError = require("../errors/movieNotFoundError");
const MovieNotLikedError = require("../errors/movieNotLikedError");
const MovieAlreadyLikedError = require("../errors/movieAlreadyLikedError");
const { Movie, User } = require("../models");

exports.findMovies = async (page, perPage, sortField, sortDirection) => {
  const { count, rows } = await Movie.findAndCountAll({
    offset: (page - 1) * perPage,
    limit: perPage,
    order: [[sortField, sortDirection]],
    attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
  });

  return { count, rows };
};

exports.createMovie = async ({ title, description, imageUrl, releaseDate }) => {
  let movie = await Movie.create({
    title: title,
    description: description,
    imageUrl: imageUrl,
    releaseDate: releaseDate,
  });
  return movie;
};

exports.updateMovie = async (movieId, updatedMovie) => {
  const movie = await findMovie(movieId);
  await movie.update(updatedMovie);

  return movie;
};

exports.deleteMovie = async (movieId) => {
  const movie = await findMovie(movieId);
  await movie.destroy();
};

exports.likeMovie = async (movieId, userId) => {
  const user = await User.findByPk(userId);
  const movie = await findMovie(movieId);

  const movieExists = await user.hasMovie(movie);

  if (movieExists) {
    throw new MovieAlreadyLikedError();
  }

  await user.addMovie(movie);
  await movie.increment("likeCount");
};

exports.disLikeMovie = async (movieId, userId) => {
  const user = await User.findByPk(userId);
  const movie = await findMovie(movieId);
  const movieExists = await user.hasMovie(movie);

  if (!movieExists) {
    throw new MovieNotLikedError();
  }

  await user.removeMovie(movie);
  await movie.decrement("likeCount");
};


const findMovie = async (movieId) => {
  const movie = await Movie.findByPk(movieId, {
    attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
  });

  if (!movie) {
    throw new MovieNotFoundError();
  }
  return movie;
};
