const EmailExistsError = require("../errors/emailExistsError");
const { User, UserLikes, Movie, Token } = require("../models");
const bcrypt = require("bcryptjs");
const {userRegisteredResponse,userAccessTokenResponse,userBannedResponse} = require("../responses/userResponse");
const paginationResponse = require("../responses/paginationResponse");
const crypto = require("crypto");
const jwt = require("jsonwebtoken");

exports.findUsers = async (page, perPage, sortField, sortDirection) => {
  const { count, rows } = await User.findAndCountAll({
    offset: (page - 1) * perPage,
    limit: perPage,
    order: [[sortField, sortDirection]],
    attributes: { exclude: ["password"] },
  });
  const response = paginationResponse(rows, count, page, perPage);

  return response;
};

exports.findUser = async (userId) => {
  const user = await User.findOne({
    where: { id: userId },
    attributes: { exclude: ["password"] },
  });

  return user;
};

exports.createUser = async ({ name, email, password, role }) => {
  const user = await User.findOne({ where: { email: email } });
  if (user) {
    throw new EmailExistsError();
  }

  const hashedPassword = await bcrypt.hash(password, 12);
  const newUser = await User.create({
    email: email,
    name: name,
    password: hashedPassword,
    role: role,
  });

  const token = generateToken(newUser);
  const response = userRegisteredResponse(newUser.id, token);

  return response;
};

exports.deleteUser = async (userId) => {
  let user = await User.findByPk(userId);
  if (!user) {
    throw new NotFoundError("User not found");
  }
  await user.destroy();
};

exports.findMoviesLikedByUser = async (
  userId,
  page,
  perPage,
  sortField,
  sortDirection
) => {
  const user = await User.findByPk(userId);
  if (!user) {
    throw new NotFoundError("User not found");
  }

  const count = await user.countMovies();
  const rows = await user.getMovies({
    offset: (page - 1) * perPage,
    limit: perPage,
    order: [[sortField, sortDirection]],
    joinTableAttributes: [],
    attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
  });

  const response = paginationResponse(rows, count, page, perPage);
  return response;
};

exports.getAccessToken = (user) => {
  const token = generateToken(user);
  const response = userAccessTokenResponse(user.id, token);
  return response;
};

exports.logout = async (token) => {
  let { jti, user } = jwt.verify(token, process.env.JWT_SECRET);
  await Token.create({
    jti: jti,
    userId: user.id,
  });

  const response = { message: "user logged out" };
  return response;
};

exports.updateBanUser = async (userId, banned) => {
  const user = await User.findByPk(userId);
  if (!user) {
    throw new NotFoundError("User not found");
  }
  await user.update({ banned: banned });
  const response = userBannedResponse(user.id, banned);
  return response;
};

/**
 * Generates new access jwt
 * @param {User} user - User object
 * @param {Number} id - user's id
 * @param {String} email - users's email
 * @param {String} role - user's role
 * @returns {String} JWT containing user data
 */
const generateToken = ({ id, email, role }) => {
  const jti = crypto.randomUUID();
  const token = jwt.sign(
    {
      user: { id: id, email: email, role: role },
      jti: jti,
    },
    process.env.JWT_SECRET,
    { expiresIn: "1h" }
  );
  return token;
};
