const { checkSchema, body } = require("express-validator");

let movieValidator = [
    body("title", "Title must be at least 5 characters").trim().isLength({ min: 5 }),
    body("description", "Description must be at least 10 characters").trim().isLength({ min: 10 }),
    body("imageUrl", "Image url must be valid").isURL(),
    body("releaseDate", "ReleaseDate must be a valid date format").isDate().toDate()
  ];


  let updatedMovieValidator = [
    body("title", "Title must be at least 5 characters").trim().isLength({ min: 5 }).optional({ checkFalsy: true }),
    body("description", "Description must be at least 10 characters").trim().isLength({ min: 10 }).optional({ checkFalsy: true }),
    body("imageUrl", "Image url must be valid").isURL().optional({ checkFalsy: true }),
    body("releaseDate", "ReleaseDate must be a valid date format").isDate().toDate().optional({ checkFalsy: true })
  ];

module.exports = { movieValidator, updatedMovieValidator };