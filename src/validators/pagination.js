const { checkSchema, query } = require("express-validator");

let paginationValidator = [
  query("page").toInt().isInt({ gt: 0 }),
  query("perPage").toInt().isInt({ min: 1, max: 30 }),
];

module.exports = paginationValidator;
