const { checkSchema, query } = require("express-validator");

const sortMoviesValidator = [
  query("sort").isIn(["title","releaseDate","likeCount","-title","-releaseDate","-likeCount"])
];

const sortUsersValidator = [
    query("sort").isIn(["email","name","-email","-name"])
  ];

module.exports = {sortMoviesValidator, sortUsersValidator};
