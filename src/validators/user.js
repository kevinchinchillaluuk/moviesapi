const { body } = require("express-validator");

const userValidator = [
    body("email", "Email must be valid format").isEmail().normalizeEmail(),
    body("name", "Name can't be empty").trim().notEmpty(),
    body("password","Password must be 7 character min").trim().isLength({ min: 7 }),
  ];

module.exports = userValidator;